const menu=document.querySelector("#menu")
const header=document.querySelector("header")
const open1=document.querySelector(".open")
const close1=document.querySelector(".close")
    function handleClick(){
        open1.classList.toggle("activeOpen")
        close1.classList.toggle("activeClose")
        header.classList.toggle("showNav")
    }

    window.addEventListener("scroll",()=>{
        header.classList.remove('showNav');
        open1.classList.remove('activeOpen');
        close1.classList.remove('activeClose');
    })

